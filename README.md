Student:
========= 
Each student has a unique student-id, first name, last name,
date of birth, phone number, gender, ssn(optional), email address, street
number, city, state, country, and zipcode.

TestType: 
========= 
TestType entity has the list of acceptable tests by the
university. It has a unique test-id and test name for each test type. 
**Note: An optional test named others can be added to this table if required.

StudentsTestType: [N:M] 
======================= 
Each Student can have many number of tests written, but not the same test twice; 
Each test type can be written by many student.  
(Ex: GRE is a test type and it can be written by more than one Student)

Application:
============
Each application has a unique application-id, application status, desired term 
of the student. Each application should be applied by only one student and each
student can apply more than one application, but not for the same department-
degree. Each application should be targeted to only one department and degree 
should be able to review by a staff of the applied department only.

Department:
===========
A department has unique department-id, department name and contact details.
Each department should offer at least one degree and courses in that that 
degree.

Degree:
=======
Each type of degree offered by the university has a unique id, degree name.
Every department should offer at least one degree (Ex: Dept. of Computer 
Science offers B.S, M.S in Data Science, M.S in AI, Ph.D and etc), likewise each
degree should be offered at least by one department.
**Note: Each degree offered by each department can have a different application
deadlines, fee per credit hour, eligibility and so on. So it need not to be
common for the whole department or a degree. This should be considered 
carefully.

Course:
=======
Each Course has a unique course-id, course name, instruction method and credit
hours. Each course should only be offered by one department-degree. (Same 
course for Undergraduate and Graduate should be identified by different course-
id since the degree is different, likewise the department). Each department can
offer more than one course.

Staff:
======
Each staff has a unique-employee-id, first and last name, address, ssn, phone
number and email. A staff should work at least in one department and each 
department should have at least one staff to work on the application review
process. A staff can manage more than one department and each and every
department should be managed by one staff. Each staff has permission only to
review the application applied to their department. Each application upon review
should have the staff-id who reviewed it.
